package animals.child.wild;

import animals.child.Wild;

public class Giraffe extends Wild {

    public Giraffe(int age, int weight, String color, boolean predator) {
        super(age, weight, color, predator);
    }
}
