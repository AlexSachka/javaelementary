package animals.child.pet;

import animals.child.Pet;

public class Hamster extends Pet {

    public Hamster(String name, int age, int weight, String color, boolean vaccinated) {
        super(name, age, weight, color, vaccinated);
    }
}
