package collection.implementation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestArray {

    @Test
    public void testCopy() {
        String[] array = {"1", "2", "4"};
        Assert.assertArrayEquals(array, Array.copy(array, array.length));
    }

    @Test
    public void testCut() {
        String[] array = {"1", "2", "4"};
        Assert.assertArrayEquals(new String[]{"1", "2"}, Array.cut(array, 2));
    }

    @Test
    public void testCutByText() {
        String[] array = {"1", "2", "4"};
        Assert.assertArrayEquals(new String[]{"1", "4"}, Array.cut(array, "2"));
    }
}
