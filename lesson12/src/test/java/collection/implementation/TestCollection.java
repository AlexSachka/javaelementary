package collection.implementation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class TestCollection {
    Collection collection;
    String[] array;


    @Before
    public void init() {
        collection = new Collection();
        array = new String[]{"1", "2", "3"};
    }

    @Test
    public void testGetByIndex() {
        String text = "1";
        collection.add(text);
        Assert.assertEquals(text, collection.get(0));
    }

    @Test
    public void testAdd() {
        Assert.assertTrue(collection.add("1"));
    }

    @Test
    public void testAddAll() {
        Assert.assertTrue(collection.addAll(array));
    }

    @Test
    public void testAddCollection() {
        Collection collection1 = new Collection();
        collection1.add("1");
        Assert.assertTrue(collection.addAll(collection1));
    }

    @Test
    public void testDelete() {
        Assert.assertFalse(collection.delete(0));
        collection.add("1");
        Assert.assertTrue(collection.delete(0));
        collection.add("1");
        Assert.assertTrue(collection.delete("1"));
    }

    @Test
    public void testContains() {
        collection.add("1");
        Assert.assertTrue(collection.contains("1"));
       // Assert.assertFalse(collection.contains("2"));
    }

    @Test
    public void testClear() {
        Assert.assertTrue(collection.clear());
    }

    @Test
    public void testTrim() {
        Assert.assertTrue(collection.trim());
    }

    @Test
    public void testCompare() {
        Collection col = new Collection();
        col.add("1");
        collection.add("1");
        Assert.assertTrue(collection.compare(col));
        col.add("11");
        Assert.assertFalse(collection.compare(col));
    }

    @Test
    public void getSum() {
        AtomicInteger sum = new AtomicInteger();
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(5);
        list.add(12);
        System.out.println(list.stream().mapToInt((x) -> x).average());

        list.stream().forEach(x -> {
            sum.addAndGet(x);
            System.out.println(sum.get());
        });
    }

    @Test
    public void getUpperCase() {
        List<String> list = new ArrayList<>();
        list.add("String1");
        list.add("String2");
        List<Pairs> list1 = list.stream().map(n -> new Pairs(n,n.toUpperCase())).collect(Collectors.toList());
        System.out.println(list1.get(0).toString());
    }

    @Test
    public void filterString(){
        List<String> list = new ArrayList<>();
        list.add("String1");
        list.add("Str2");
        list.add("qwer");
        list.add("qwqer");
        list.stream().filter(n-> n.length() == 4).filter(n-> n.equals(n.toLowerCase())).forEach(System.out::println);
    }



}



class Pairs{
    private String before;
    private String after;


    public Pairs(String before, String after) {
        this.before = before;
        this.after = after;
    }
    @Override
    public String toString() {
        return "Pairs{" +
                "before='" + before + '\'' +
                ", after='" + after + '\'' +
                '}';
    }
}

