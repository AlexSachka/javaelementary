import java.io.Console;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        // Task1
        // При помощи цикла for вывести на экран нечетные числа от 1 до 99.
        //Task1.printEvenNumber();
        //Task1.printEvenNumber2();

        //Task2
        // Дано число n при помощи цикла for посчитать факториал n!
        //Task2.printFactorial(5);

        //Task3
        //Перепишите программы с использованием цикла while.
        //Task3.printFactorialUseWhile(5);
        //Task3.printEvenNumber();
        //Task3.printEvenNumber2();

        //Task4
        //Перепишите программы с использованием цикла do - while.
        // Task4.printFactorialUseDoWhile(5);
        //Task4.printEvenNumber();
        //Task4.printEvenNumber2();

        //Task5
        // Даны переменные x и n вычислить x^n.
        //Task5.pow(3,3);

        // Task6
        // Вывести 10 первых чисел последовательности 0, -5,-10,-15..
        //Task6.printNumbersWithStepFive();

        //Task7
        //Необходимо вывести на экран таблицу умножения на Х: (любое число вводимое из консоли)
        //Task7.printMultiplicationTable(4);

        //Task8
        // Создайте массив, содержащий 10 первых нечетных чисел.
        // Выведете элементы массива на консоль в одну строку, разделяя запятой.
        //Task8.printArray();

        //Task9
        // Дан массив размерности N, найти наименьший элемент массива и вывести
        // на консоль (если наименьших элементов несколько — вывести их все).
       /* int[] array = {66, 55, 4, 0, 22, 0, 0};
        Task9.printMinNumbersInArray(array);*/

        //Task10
        //В массиве из задания 9. найти наибольший элемент.
       /* int[] array = {66, 55, 4, 0, 22, 0, 0};
        Task10.printMaxArrayNumber(array);*/

        //Task11
        // Поменять наибольший и наименьший элементы массива местами. Пример: дан массив {4, -5, 0, 6, 8}.
        // После замены будет выглядеть {4, 8, 0, 6, -5}.
       /* int[] array = {66, 55, 4, 1, 22, 10, 7};
        Task11.changeMinMaxNumbersInArray(array);
        System.out.println(Arrays.toString(array));*/

        //Task12
        // Найти среднее арифметическое всех элементов массива.
        /*int[] array = {66, 55, 4, 1, 22, 10, 7};
        Task12.printArrayAverage(array);*/

        //Task13
        //(*)Вывести на экран шахматную
        // доску 8х8 в виде 2 мерного массива (W - белые клетки , B - черные клети)
        //Task13.printChessField();
        int range = 10000000;
        int a =  (int)(Math.random() * range);
        System.out.println(a);

    }
}

class Task1 {
    public static void printEvenNumber() {
        for (int i = 1; i <= 99; i += 2) {
            System.out.println(i);
        }
    }

    public static void printEvenNumber2() {
        for (int i = 0; i <= 99; i++) {
            if (i % 2 != 0) {
                System.out.println(i);
            }
        }
    }
}

class Task2 {
    public static void printFactorial(int number) {
        int factorial = 1;
        for (int i = 2; i <= number; i++) {
            factorial *= i;
        }
        System.out.println(factorial);
    }
}

class Task3 {
    public static void printFactorialUseWhile(int number) {
        int factorial = 1;
        int i = 2;
        while (i <= number) {
            factorial *= i;
            i++;
        }
        System.out.println(factorial);
    }

    public static void printEvenNumber() {
        int i = 1;
        while (i <= 99) {
            System.out.println(i);
            i += 2;
        }
    }

    public static void printEvenNumber2() {
        int i = 0;
        while (i <= 99) {
            if (i % 2 != 0) {
                System.out.println(i);
            }
            i++;
        }
    }
}

class Task4 {
    public static void printFactorialUseDoWhile(int number) {
        int factorial = 1;
        int i = 2;
        do {
            factorial *= i;
            i++;
        }
        while (i <= number);
        System.out.println(factorial);
    }

    public static void printEvenNumber() {
        int i = 1;
        do {
            System.out.println(i);
            i += 2;
        }
        while (i <= 99);
    }

    public static void printEvenNumber2() {
        int i = 1;
        do {
            if (i % 2 != 0) {
                System.out.println(i);
            }
            i++;
        }
        while (i <= 99);
    }
}

class Task5 {
    public static void pow(int number, int power) {
        int result = number;
        for (int i = 1; i < power; i++) {
            number *= result;
        }
        System.out.println(number);
    }
}

class Task6 {
    public static void printNumbersWithStepFive() {
        int counter = 0;
        int i = 0;
        while (true) {
            System.out.println(i);
            i -= 5;
            counter++;
            if (counter == 10) {
                break;
            }
        }
    }
}

class Task7 {
    public static void printMultiplicationTable(int number) {
        int result = 0;
        for (int i = 1; i < 10; i++) {
            result = i * number;
            System.out.println(number + " * " + i + " = " + result);
        }
    }
}

class Task8 {
    public static void printArray() {
        int[] array = new int[10];
        int number = 1;
        for (int i = 0; i < array.length; i++) {
            array[i] = number;
            number += 2;
        }
        for (int i : array) {
            System.out.print(i + ", ");
        }
    }
}

class Task9 {
    public static void printMinNumbersInArray(int[] array) {
        String minNumbers = "";
        int minNumber = array[0];
        int index = 0;
        // Нахожу минимальный елемент масива
        for (int i = 1; i < array.length; i++) {
            if (minNumber >= array[i]) {
                minNumber = array[i];
                index = i;
            }
        }
        minNumbers += minNumber + ", ";
        // Проверяю наличия такого же елемента в масиве и записываю все переменую стринг
        for (int i = 0; i < array.length; i++) {
            if (minNumber == array[i] && index != i) {
                minNumbers += array[i] + ", ";
            }
        }
        System.out.println(minNumbers);
    }
}

class Task10 {
    public static void printMaxArrayNumber(int[] array) {
        int maxNumber = array[0];
        // Нахожу минимальный елемент масива
        for (int i = 1; i < array.length; i++) {
            if (maxNumber <= array[i]) {
                maxNumber = array[i];
            }
        }
        System.out.println(maxNumber);
    }
}

class Task11 {
    public static void changeMinMaxNumbersInArray(int[] array) {
        int minNumber = array[0];
        int minIndex = 0;
        int maxNumber = array[0];
        int maxIndex = 0;
        for (int i = 1; i < array.length; i++) {
            if (minNumber > array[i]) {
                minNumber = array[i];
                minIndex = i;
            } else if (maxNumber < array[i]) {
                maxNumber = array[i];
                maxIndex = i;
            }
        }
        int index = array[minIndex];
        array[minIndex] = array[maxIndex];
        array[maxIndex] = index;
    }
}

class Task12 {
    public static void printArrayAverage(int[] array) {
        double sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        System.out.println("Array average = " + sum / array.length);
    }
}

class Task13 {
    public static void printChessField() {
        String[][] array = new String[8][8];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (i % 2 == 0 && j % 2 == 0) {
                    array[i][j] = "B ";
                } else if (i % 2 == 0 && j % 2 != 0) {
                    array[i][j] = "W ";
                } else if (i % 2 != 0 && j % 2 != 0) {
                    array[i][j] = "B ";
                } else if (i % 2 != 0 && j % 2 == 0) {
                    array[i][j] = "W ";
                }
            }
        }
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j]);
            }
            System.out.println();
        }
    }
}


