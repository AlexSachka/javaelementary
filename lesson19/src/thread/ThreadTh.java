package thread;

import cashMachine.CashMachine;

public class ThreadTh extends Thread {
    CashMachine cashMachine;

    public ThreadTh(String name, CashMachine cashMachine) {
        super(name);
        this.cashMachine = cashMachine;
    }

    @Override
    public void run() {
        synchronized (cashMachine){
            for (int i = 0; i < 3; i++) {
                System.out.println("Thread name is " + getName() +
                        ", withdrawal sum is " + cashMachine.withdrawalMoney(10) +
                        ", added money " + cashMachine.addMoney(100) + ", balance "
                        + cashMachine.getMoney());
            }
        }
    }
}
