package cashMachine;

public class CashMachine {
    static double money = 1000;

    public double addMoney(double money) {
        this.money += money;
        return money;
    }

    public double withdrawalMoney(double money) {
        if (this.money - money < 0) {
            System.err.println("Not enough money");
            return -1;
        }
        this.money -= money;
        return money;
    }

    public double getMoney() {
        return money;
    }
}
