import cashMachine.CashMachine;
import thread.ThreadTh;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        CashMachine cashMachine = new CashMachine();
        ThreadTh thread1 = new ThreadTh("Vasy",cashMachine);
        ThreadTh thread2 = new ThreadTh("Koly",cashMachine);
        thread1.start();
        thread2.start();

    }
}
