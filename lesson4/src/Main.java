public class Main {
    public static void main(String[] args) {
        double gas = 0;
        final double gasPrise = 24.5;
        Car car1 = new Car(100, 100, 10);
        // Еду до кривого Озера 178 км, определяю сколько нужно дозаправитсься и заправляюсь
         gas+= car1.determineGasToRefuel(178);
         System.out.println("Need to refuel = " + car1.determineGasToRefuel(178) + " liters");
        //Еду от кривого Озера до Жашков 154км. определяю сколько нужно дозаправиться
        gas+= car1.determineGasToRefuel(154);
        System.out.println("Need to refuel = " + car1.determineGasToRefuel(154) + " liters");
        // Доезжаю до Киев считаю стоимость и остаток топлива
        System.out.println("Cost of trip " + gas * gasPrise);
        System.out.println("Gas remaining after trip " + car1.determineRemainingGas(150));

    }
}

class Car {
    private int volume;
    private int gasBalance;
    private int gasPerHundredKilometers;


    public Car(int volume, int gasBalance, int gasPerHundredKilometers) {
        this.volume = volume;
        this.gasBalance = gasBalance;
        this.gasPerHundredKilometers = gasPerHundredKilometers;
    }

    // Залить полный бак с учетом объема  остатка
    public void refuelCar() {
        gasBalance += (volume - gasBalance);
    }

    // определить остаток топлива по преодолении N км
    public double determineRemainingGas(double kilometers) {
        double totalGas = kilometers / gasPerHundredKilometers;
        return gasBalance - totalGas;
    }

    //определить сколько надо дозаправить топлива при преодолении N км
    public double determineGasToRefuel(double kilometers) {
        double totalGas = kilometers / gasPerHundredKilometers;
        return volume - (gasBalance - totalGas);
    }
}

