package task1;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Task1 {
    //  Напишите метод, который на вход получает
    // коллекцию объектов, а возвращает коллекцию уже без дубликатов.
    public static List<String> removeDuplicate(List<String> list){
        List<String> list1 = list;
        Set<String> set = new HashSet<>();
        set.addAll(list);
        list.clear();
        list.addAll(set);
        return list;
    }
}
