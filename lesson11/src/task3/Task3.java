package task3;

public class Task3 {
    // написать программу для вычисления корней квадратного уравнения
    private double a;
    private double b;
    private double c;
    private double resultX;
    private double resultX2;

    public Task3(double a) {
        this.a = a;
    }

    public Task3(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public Task3(double a, Double c) {
        this.a = a;
        this.c = c;
    }

    public Task3(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double [] getResult() {
        if (a == 0) {
            System.out.println("Wrong data");
            return new double[]{resultX};
        }
        else if (b == 0 && c == 0)
            return new double[]{0};
        else if (c == 0)
            return new double[]{quadraticEquationWithExistB()};
        else if (b == 0)
            return new double[] {quadraticEquationWithExistC()} ;
        else
            return quadraticEquation();
    }

    private double quadraticEquationWithExistB() {
        return (b / a) * -1;
    }

    private double quadraticEquationWithExistC() {
        return Math.sqrt(b / a) * -1;
    }

    private double [] quadraticEquation() {
        double discriminant = Math.pow(b, 2) - 4 * a * c;
        if (discriminant < 0){
            System.out.println("No result discriminant < 0");
            return new double[]{};
        }
        resultX = (-(b) + Math.sqrt(discriminant)) / (2 * a);
        resultX2 = (-(b) - Math.sqrt(discriminant)) / (2 * a);
        return new double[]{resultX,resultX2};
    }
}
