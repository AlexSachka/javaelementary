import task1.Task1;
import task2.Task2;
import task3.Task3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        // Task1
        /*List<String> list1 = new ArrayList<>();
        list1.add("1");
        list1.add("1");
        list1.add("2");
        List<String> list2 =  Task1.removeDuplicate(list1);
         for (String a : list2){
            System.out.println(a);
        }*/

        // Task2
       /* String[] array22 = {"1", "2", "3", "4"};
        Task2 task2 = new Task2(array22);
        task2.next();
        task2.remove();
        System.out.println(task2.size);*/

        // Task3
        Task3 task = new Task3(5,66,2);
        System.out.println(Arrays.toString(task.getResult()));
        Task3 task1 = new Task3(5,77);
        System.out.println(Arrays.toString(task1.getResult()));
    }
}
