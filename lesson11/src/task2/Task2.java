package task2;

import java.util.Iterator;


public class Task2 implements Iterator {
    private String[] array;
    public int size;
    private int index;

    public Task2(String[] array) {
        this.array = array;
        this.size = array.length;
    }


    @Override
    public boolean hasNext() {
        return index < size;
    }

    @Override
    public String next() {
        return array[index++];
    }

    @Override
    public void remove() {
        if (index < size) {
            System.arraycopy(array, index, array, 0, array.length - 1);
            size--;
        }
    }


    // Второй способ это если имплементрировать Iterable интерфейс
    // индекс внутри анонимного класса
   /* @Override
    public Iterator iterator() {
        Iterator it = new Iterator() {
            private int index = 0;

            @Override
            public boolean hasNext() {
                return index < size;
            }

            @Override
            public String next() {
                return array[index++];
            }

            @Override
            public void remove() {
                if (index < size) {
                    System.arraycopy(array, index, array, 0, array.length - 1);
                }
                size--;
            }
        };
        return it;
    }*/
}

