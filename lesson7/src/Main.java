import collection.implementation.Array;
import collection.implementation.Collection;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        // Добавляем елемент, проверяем размер, вытаскиеваем по индексу
        // Проверяем на contains.
        // Проверяем clear
       /* Collection collection = new Collection();
        collection.add("Hillel");
        System.out.println(collection.size());
        System.out.println(collection.get(0));
        System.out.println(collection.contains("Hillel"));
        collection.clear();
        System.out.println(collection.size());*/

        // Удаляем елемент по индексу, потом по текстуж
       /* Collection collection = new Collection();
        collection.add("Hillel1");
        collection.add("Hillel2");
        Collection.printCollection(collection);
        collection.delete(0);
        System.out.println("---------Delete----------");
        Collection.printCollection(collection);
        collection.delete("Hillel2");
        System.out.println("---------Delete----------");
        Collection.printCollection(collection);*/

        // Добавление в колекцию масива с даными
        // Добавление в колекцию другую колекцию
       /* Collection collection = new Collection();
        Collection collection1 = new Collection();
        String [] array = {"qwer","asdf"};
        collection.add("1");
        collection1.add("2");
        collection.addAll(array);
        System.out.println("Вывод колекции + масив");
        Collection.printCollection(collection);
        System.out.println("Вывод колекции + колекция");
        collection.addAll(collection1);
        Collection.printCollection(collection);*/

        // Проверка на compare
       /* Collection collection = new Collection();
        Collection collection1 = new Collection();
        collection.add("1");
        collection.add("2");
        collection1.add("2");
        collection1.add("1");
        collection1.add("1");
        System.out.println(collection.compare(collection1)) ;*/

       // Проверка trim
       /* Collection collection = new Collection();
        collection.add("5");
        System.out.println(collection.get(5)); // вывидет null так как масив в конструкторе на 10 елементов
        collection.trim();
        System.out.println(collection.get(5));*/// ArrayIndexOutOfBoundsException

        // Тест
        // 100 разз
       /* Collection collection = new Collection();
        long start = System.nanoTime();
        for (int i = 0; i < 100; i++) {
            collection.add("1");
        }
        long end = System.nanoTime();
        long elapsedTime = end - start;
        double elapsedTimeInSecond = (double) elapsedTime / 1_000_000_000;
        System.out.println(elapsedTimeInSecond);
        System.out.println("---------------------");
        Collection collection1 = new Collection();
        long start1 = System.nanoTime();
        for (int i = 0; i < 100; i++) {
            collection1.add1("1");
        }
        long end1 = System.nanoTime();
        long elapsedTime1 = end1 - start1;
        double elapsedTimeInSecond1 = (double) elapsedTime1 / 1_000_000_000;
        System.out.println(elapsedTimeInSecond1);*/

        // 10000 раз
      /*  Collection collection = new Collection();
        long start = System.nanoTime();
        for (int i = 0; i < 10000; i++) {
            collection.add("1");
        }
        long end = System.nanoTime();
        long elapsedTime = end - start;
        double elapsedTimeInSecond = (double) elapsedTime / 1_000_000_000;
        System.out.println(elapsedTimeInSecond);
        System.out.println("---------------------");
        Collection collection1 = new Collection();
        long start1 = System.nanoTime();
        for (int i = 0; i < 10000; i++) {
            collection1.add1("1");
        }
        long end1 = System.nanoTime();
        long elapsedTime1 = end1 - start1;
        double elapsedTimeInSecond1 = (double) elapsedTime1 / 1_000_000_000;
        System.out.println(elapsedTimeInSecond1);*/

        // 1000000 раз
        Collection collection = new Collection();
        long start = System.nanoTime();
        for (int i = 0; i < 1000000; i++) {
            collection.add("1");
        }
        long end = System.nanoTime();
        long elapsedTime = end - start;
        double elapsedTimeInSecond = (double) elapsedTime / 1_000_000_000;
        System.out.println(elapsedTimeInSecond);
        System.out.println("---------------------");
        Collection collection1 = new Collection();
        long start1 = System.nanoTime();
        for (int i = 0; i < 1000000; i++) {
            collection1.add1("1");
        }
        long end1 = System.nanoTime();
        long elapsedTime1 = end1 - start1;
        double elapsedTimeInSecond1 = (double) elapsedTime1 / 1_000_000_000;
        System.out.println(elapsedTimeInSecond1);
    }
}

