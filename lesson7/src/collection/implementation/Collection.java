package collection.implementation;

public class Collection implements collection.interfaces.CustomCollection {
    private String[] array;
    private int size = 0;


    public Collection() {
        array = new String[10];
    }

    @Override
    public boolean add(String str) {
        String[] array2;
        boolean result = false;
        try {
            array[size] = str;
            result = true;
            size++;
        } catch (ArrayIndexOutOfBoundsException e) {
            array2 = Array.copy(array, (int) (array.length + (array.length * 0.6)));
            array2[size] = str;
            size++;
            array = Array.copy(array2, array2.length);
        }
        return result;
    }

    // Метод умеличивает масив на 1 для теста
    public boolean add1(String str) {
        String[] array2;
        boolean result = false;
        try {
            array[size] = str;
            result = true;
            size++;
        } catch (ArrayIndexOutOfBoundsException e) {
            array2 = Array.copy(array, (array.length + 1));
            array2[size] = str;
            size++;
            array = Array.copy(array2, array2.length);
        }
        return result;
    }


    @Override
    public boolean addAll(String[] strArr) {
        String[] array2;
        boolean result = false;
        for (int i = 0; i < strArr.length; i++) {
            if (strArr[i] == null) {
                continue;
            }
            try {
                array[size] = strArr[i];
                result = true;
                size++;
            } catch (ArrayIndexOutOfBoundsException e) {
                array2 = Array.copy(array, (int) (array.length + (array.length * 0.6)));
                array2[size] = strArr[i];
                size++;
                array = Array.copy(array2, array2.length);
            }
        }
        return result;
    }

    @Override
    public boolean addAll(Collection strColl) {
        String[] array2;
        boolean result = false;
        for (int i = 0; i < strColl.size(); i++) {
            try {
                array[size] = strColl.get(i);
                result = true;
                size++;
            } catch (ArrayIndexOutOfBoundsException e) {
                array2 = Array.copy(array, (int) (array.length + (array.length * 0.6)));
                array2[size] = strColl.get(i);
                size++;
                array = Array.copy(array2, array2.length);
            }
        }
        return result;
    }

    @Override
    public boolean delete(int index) {
        String[] array2;
        boolean result = false;
        if (index > size) {
            return false;
        } else {
            array2 = Array.cut(array, index);
            array = Array.copy(array2, array2.length);
            result = true;
            size--;
        }
        return result;
    }

    @Override
    public boolean delete(String str) {
        String[] array2;
        boolean result = false;
        if (str == null) {
            return false;
        } else {
            array2 = Array.cut(array, str);
            array = Array.copy(array2, array2.length);
            result = true;
            size--;
        }
        return result;
    }

    @Override
    public String get(int index) {
        return array[index];
    }

    @Override
    public boolean contains(String str) {
        boolean result;
        result = false;
        for (int i = 0; i < size(); i++) {
            if (array[i].equals(str))
                result = true;
        }
        return result;
    }

    @Override
    public boolean clear() {
        array = new String[10];
        size = 0;
        return true;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean trim() {
        String[] array2;
        array2 = new String[size];
        for (int i = 0; i < size; i++) {
            array2[i] = array[i];
        }
        array = Array.copy(array2, array2.length);
        return true;
    }

    @Override
    public boolean compare(Collection coll) {
        int count = 0;
        if (size == coll.size) {
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < coll.size; j++) {
                    if (array[i].equals(coll.get(j))) {
                        count++;
                        break;
                    }
                }
            }
        }
        return count == size;
    }

    public static void printCollection(Collection collection) {
        for (int i = 0; i < collection.size; i++) {
            System.out.println(collection.get(i));
        }
    }
}
