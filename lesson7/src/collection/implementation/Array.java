package collection.implementation;

public class Array {

    public static String[] copy(String[] arr, int length) {
        String[] array = new String[length];
        for (int i = 0; i < arr.length; i++) {
            array[i] = arr[i];
        }
        return array;
    }

    public static String[] cut(String[] arr, int index) {
        String[] array = new String[arr.length - 1];
        int newIndex = 0;
        for (int i = 0; i < array.length; i++) {
            if (i != index) {
                array[newIndex] = arr[i];
                newIndex++;
            }
        }
        return array;
    }
    public static String [] cut(String[] arr, String text){
        String[] array = new String[arr.length - 1];
        int newIndex = 0;
        for (int i = 0; i < array.length; i++) {
            if (arr[i] != text) {
                array[newIndex] = arr[i];
                newIndex++;
            }
        }
        return  array;
    }
}
