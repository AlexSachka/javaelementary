import implementation.Bot;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestBot {
    Bot bot;
    @Before
    public void precondition(){
        bot = new Bot();
    }
    @Test
    public void testChooseAction(){
        String actual = bot.chooseAction();
        switch (actual){
            case "ROCK":
                Assert.assertEquals("Wrong action","ROCK", actual);
                break;
            case "SCISSORS":
                Assert.assertEquals("Wrong action","SCISSORS",actual);
                break;
            case "PAPER":
                Assert.assertEquals("Wrong action","PAPER",actual);
                break;
        }
    }
}
