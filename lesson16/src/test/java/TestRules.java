import implementation.Rules;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestRules {
    Rules rules;

    @Before
    public void precondition() {
        rules = new Rules();

    }

    @Test
    public void testGameResultRock() {
        Assert.assertEquals("Wrong game result", "Выйграли ВЫ, бот показал SCISSORS. "
                , rules.gameResult("ROCK","SCISSORS"));
        Assert.assertEquals("Wrong game result", "Выйграл Бот, показал PAPER. "
                , rules.gameResult("ROCK","PAPER"));
        Assert.assertEquals("Wrong game result", "У вас ничья, Бот показал ROCK. "
                , rules.gameResult("ROCK","ROCK"));
    }

    @Test
    public void testGameResultScissors() {
        Assert.assertEquals("Wrong game result", "Выйграли ВЫ, бот показал PAPER. "
                , rules.gameResult("SCISSORS","PAPER"));
        Assert.assertEquals("Wrong game result", "Выйграл Бот, показал ROCK. "
                , rules.gameResult("SCISSORS","ROCK"));
        Assert.assertEquals("Wrong game result", "У вас ничья, Бот показал SCISSORS. "
                , rules.gameResult("SCISSORS","SCISSORS"));
    }

    @Test
    public void testGameResultPaper() {
        Assert.assertEquals("Wrong game result", "Выйграли ВЫ, бот показал ROCK. "
                , rules.gameResult("PAPER","ROCK"));
        Assert.assertEquals("Wrong game result", "Выйграл Бот, показал SCISSORS. "
                , rules.gameResult("PAPER","SCISSORS"));
        Assert.assertEquals("Wrong game result", "У вас ничья, Бот показал PAPER. "
                , rules.gameResult("PAPER","PAPER"));
    }

}
