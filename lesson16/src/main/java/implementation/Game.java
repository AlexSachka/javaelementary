package implementation;

import java.io.*;
import java.util.Scanner;

public class Game {
    Rules rules;
    Bot bot;
    Scanner sc;
    private String dir;
    private String fileName;
    File result;
    File gameResult;

    public Game() {
        createFile();
    }

    public void startGame() {
        bot = new Bot();
        sc = new Scanner(System.in);
        rules = new Rules();
        System.out.println("Please choose amount of game");
        // todo: почему когда использую nextInt не записывается значение
        String gameCount = sc.nextLine();
        for (int i = 0; i < Integer.parseInt(gameCount); i++) {
            System.out.println("Введите команду: ROCK, SCISSORS, PAPER, если вы хотите " +
                    "окончить игру напишите end");
            String text = sc.nextLine();
            if (text.equalsIgnoreCase("end")) {
                System.out.println("Finish game");
                break;
            }
            String gameResult = rules.gameResult(text, bot.chooseAction());
            saveGameResult(gameResult);
            System.out.println(gameResult);
        }
    }

    public void printGameResults() {
        try (FileInputStream fileIn = new FileInputStream(gameResult);
             InputStreamReader reader = new InputStreamReader(fileIn, "UTF-8")) {
            int i;
            while ((i = reader.read()) != -1) {
                if ((char) i == '.') {
                    System.out.println();
                }
                System.out.print((char) i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean saveGameResult(String text) {
        try (FileWriter fileOut = new FileWriter(gameResult, true)) {
            fileOut.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    private void createFile() {
        dir = "D:\\courseJavaElementary\\lesson16" +
                "\\src\\main\\java\\gameResult\\";
        fileName = ("gameResult" + ((int) Math.random()) * 1000) + ".txt";
        result = new File(dir, fileName);
        try {
            result.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        gameResult = new File(dir + fileName);
    }
}
