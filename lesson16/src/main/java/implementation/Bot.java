package implementation;

public class Bot {
    public String chooseAction() {
        final Actions[] AC = {Actions.PAPER, Actions.ROCK, Actions.SCISSORS};
        int random = (int) (Math.random() * 3);
        return String.valueOf(AC[random]);
    }
}
