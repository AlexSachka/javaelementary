package implementation;

public class Rules {
    public String gameResult(String userAction, String botAction){
        String userWin = "Выйграли ВЫ, бот показал " + botAction + ". ";
        String botWin = "Выйграл Бот, показал " + botAction + ". ";
        String result = "";
        if (userAction.equalsIgnoreCase(botAction)){
            result = "У вас ничья, Бот показал " + botAction + ". ";
        } else if (userAction.equalsIgnoreCase("ROCK") &&
                botAction.equalsIgnoreCase("SCISSORS")){
            result = userWin;
        } else if (userAction.equalsIgnoreCase("ROCK") &&
                botAction.equalsIgnoreCase("PAPER")){
            result = botWin;
        } else if (userAction.equalsIgnoreCase("SCISSORS") &&
                botAction.equalsIgnoreCase("ROCK")){
            result = botWin;
        } else if (userAction.equalsIgnoreCase("SCISSORS") &&
                botAction.equalsIgnoreCase("PAPER")){
            result = userWin;
        } else if (userAction.equalsIgnoreCase("PAPER") &&
                botAction.equalsIgnoreCase("ROCK")){
            result = userWin;
        } else if (userAction.equalsIgnoreCase("PAPER") &&
                botAction.equalsIgnoreCase("SCISSORS")){
            result = botWin;
        } else {
            result = "Something went wrong";
        }
        return result;
    }
}
