import implementation.Game;

public class Main {
    public static void main(String[] args) {
        Game game = new Game();
        game.startGame();
        // Добавил еще метод получить весь результат
        //game.printGameResults();
    }
}
