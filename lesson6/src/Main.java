import animals.child.pet.Cat;
import animals.child.pet.Dog;
import animals.child.pet.Fish;
import animals.child.pet.GuideDog;
import animals.child.wild.Wolf;

public class Main {
    public static void main(String[] args) {
        Wolf wolf = new Wolf(12, 50, "grey", true);
        wolf.voice();
        Dog dog = new Dog("Barsik", 4, 29, "black", true);
        dog.voice();
        Cat cat = new Cat("Murka", 4, 7, "white", true);
        cat.voice();
        GuideDog guideDog = new GuideDog("Barsik", 4,
                29, "black", true, true);
        guideDog.goHome();
        Fish fish = new Fish("Nemo", 4,
                29, "black", true);
        fish.voice();
        /////// Мои методы
        dog.biteCat(cat);
        wolf.eat();
    }
}
