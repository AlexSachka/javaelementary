package animals;

public class Animals {
    public static int id = 0;
    protected int age;
    protected int weight;
    protected String color;

    public Animals(int age, int weight, String color) {
        this.age = age;
        this.weight = weight;
        this.color = color;
        id++;
    }

    public void voice() {
        System.out.println("Hello");
    }

    @Override
    public String toString() {
        return "Animals{" +
                "age=" + age +
                ", weight=" + weight +
                ", color='" + color + '\'' +
                '}';
    }
}
