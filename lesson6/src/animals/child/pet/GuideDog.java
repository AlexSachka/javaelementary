package animals.child.pet;

public class GuideDog extends Dog {
    private boolean isTrained;
    public GuideDog(String name, int age, int weight, String color, boolean vaccinated, boolean isTrained) {
        super(name, age, weight, color, vaccinated);
        this.isTrained = isTrained;
    }

    public void goHome(){
        if(isTrained){
            System.out.println("Go home");
        } else {
            System.out.println("I can't catch you home, I'm not trained");
        }
    }
}
