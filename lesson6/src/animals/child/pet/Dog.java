package animals.child.pet;

import animals.child.Pet;

public class Dog extends Pet {
    public Dog(String name, int age, int weight, String color, boolean vaccinated) {
        super(name, age, weight, color, vaccinated);
    }

    public void biteCat(Cat cat){
        System.out.println("I bite " + cat.getName());
    }

    @Override
    public void voice() {
        System.out.println("Woof");
    }
}
