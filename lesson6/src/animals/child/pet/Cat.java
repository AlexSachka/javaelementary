package animals.child.pet;

import animals.child.Pet;

public class Cat extends Pet {
    public Cat(String name, int age, int weight, String color, boolean vaccinated) {
        super(name, age, weight, color, vaccinated);
    }


    @Override
    public void voice() {
        System.out.println("Meow");
    }
}
