package animals.child.pet;

import animals.child.Pet;

public class Fish extends Pet {

    public Fish(String name, int age, int weight, String color, boolean vaccinated) {
        super(name, age, weight, color, vaccinated);
    }

    @Override
    public void voice() {
        System.out.println(".............");
    }
}
