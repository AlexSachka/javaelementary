package animals.child;

import animals.Animals;
import animals.interfaces.Instincts;

public class Wild extends Animals implements Instincts {
    protected boolean predator;
    public Wild(int age, int weight, String color, boolean predator) {
        super(age, weight, color);
        this.predator = predator;
    }

    @Override
    public void eat() {
        if (predator){
            System.out.println("I eating meat now");
        } else {
            System.out.println("Give me grass");
        }
    }

    @Override
    public void voice() {
        if (predator) {
            printWildAndAngry();
        } else {
            printWild();
        }
    }
    private void printWild() {
        System.out.println("Hello, I am a wild animal");
    }

    private void printWildAndAngry() {
        System.out.println("Hello, I am a wild animal and angry");
    }

}
