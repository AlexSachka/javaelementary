package animals.child;

import animals.Animals;
import animals.interfaces.Instincts;

public class Pet extends Animals implements Instincts {
    protected boolean vaccinated;
    protected String name;

    public Pet(String name, int age, int weight, String color, boolean vaccinated) {
        super(age, weight, color);
        this.vaccinated = vaccinated;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void voice(){
        System.out.println("Hello my name is " + name);
    }

    @Override
    public void eat() {
        System.out.println("I'm eating now");
    }
}
