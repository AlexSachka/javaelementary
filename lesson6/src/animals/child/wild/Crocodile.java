package animals.child.wild;

import animals.child.Wild;

public class Crocodile extends Wild {

    public Crocodile(int age, int weight, String color, boolean predator) {
        super(age, weight, color, predator);
    }
}
