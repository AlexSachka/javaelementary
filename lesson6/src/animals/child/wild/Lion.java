package animals.child.wild;

import animals.child.Wild;

public class Lion extends Wild {
    public Lion(int age, int weight, String color, boolean predator) {
        super(age, weight, color, predator);
    }
}
