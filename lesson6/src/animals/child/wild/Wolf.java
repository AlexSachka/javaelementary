package animals.child.wild;

import animals.child.Wild;

public class Wolf extends Wild {
    public Wolf(int age, int weight, String color, boolean predator) {
        super(age, weight, color, predator);
    }
}
