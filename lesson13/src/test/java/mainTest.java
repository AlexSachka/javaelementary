import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class mainTest {
    @Test
    public void testGetAverage() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(5);
        list.add(12);
        Stream.getAverage(list);
    }

    @Test
    public void getUpperCase() {
        List<String> list = new ArrayList<>();
        list.add("String1");
        list.add("String2");
        List<Pairs> list1 = Stream.getUpperCase(list);
        Assert.assertEquals("Strings not equals", list1.get(0).getBefore(), "String1");
        Assert.assertEquals("Strings not equals", list1.get(0).getAfter(), "STRING1");

    }

    @Test
    public void filterString() {
        List<String> list = new ArrayList<>();
        list.add("String1");
        list.add("Str2");
        list.add("qwer");
        list.add("qwqer");
        Stream.filterString(list);
    }
}
