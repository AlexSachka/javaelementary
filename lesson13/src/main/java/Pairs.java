public class Pairs {
    private String before;
    private String after;

    public Pairs(String before, String after) {
        this.before = before;
        this.after = after;
    }

    public String getBefore() {
        return before;
    }

    public String getAfter() {
        return after;
    }

    @Override
    public String toString() {
        return "Pairs{" +
                "before='" + before + '\'' +
                ", after='" + after + '\'' +
                '}';
    }
}
