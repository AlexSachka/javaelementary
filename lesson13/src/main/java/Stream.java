import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Stream {

    public static void getAverage(List<Integer> list) {
        System.out.println(list.stream().mapToInt((x) -> x).average());
    }

    public static List<Pairs> getUpperCase(List<String> list){
        List<Pairs> list1 = list.stream().map(n -> new Pairs(n,n.toUpperCase())).collect(Collectors.toList());
        return list1;
    }

    public static void filterString(List<String> list){
        list.stream().filter(n-> n.length() == 4).filter(n-> n.equals(n.toLowerCase())).forEach(System.out::println);
    }
}
