import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static final Logger loggerWarning = LoggerFactory.getLogger("logger.warning");
    private static final Logger loggerError = LoggerFactory.getLogger("logger.error");
    private static final Logger loggerInfo = LoggerFactory.getLogger("logger.info");
    public static void main(String[] args) {
        loggerWarning.warn("I'm warning");
        loggerError.error("I'm error");
        loggerInfo.info("I'm info");

    }
}
