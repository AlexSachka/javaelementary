package slq;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static java.sql.DriverManager.getConnection;

public class DataBase {
    public static final String URL = "jdbc:mysql://localhost:3306/student?useSSL=false";
    public static final String userName = "root";
    public static final String password = "root";
    Connection connection;
    Statement statement;
    ResultSet set;

    public DataBase() throws SQLException {
        this.connection = getConnection(URL, userName, password);
        this.statement = connection.createStatement();
    }

    public void getStudentsInfo(String query) {
        try {
            set = statement.executeQuery(query);
            int count = set.getMetaData().getColumnCount();
            while (set.next()) {
                for (int i = 1; i <= count; i++) {
                    System.out.println(set.getMetaData().getColumnName(i)
                            + ": " + set.getString(i));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
