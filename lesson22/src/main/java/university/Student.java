package university;

import slq.DataBase;

import java.sql.SQLException;

public class Student {
    int studentId;
    String fullName;
    int groupName;
    String yearOfReceipt;

    public Student(int studentId, String fullName, int groupName, String yearOfReceipt) {
        this.studentId = studentId;
        this.fullName = fullName;
        this.groupName = groupName;
        this.yearOfReceipt = yearOfReceipt;
    }
}
