import slq.DataBase;
import university.Student;

import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        // Список студентов всех
        String getAllUsers = "select * from student.student";
        // Список студентов обучающихся в одной группе
        String getUsersFromOneGroup = "SELECT * FROM student.student where groupname in " +
                "(select groupname from student.student GROUP BY groupname having count(*)>1)";
        // Список студентов одного года поступления
        String getUserOneDataYearOfReceipt = "SELECT * FROM student.student where Yearofreceipt in (" +
                "select Yearofreceipt from student.student GROUP BY groupname having count(*)>1)";
        // Все предметы студента с оценками и преподавателями
        String getLessonsWithBallsAndTeachers = "SELECT student.Fullname, balls.rating, lessons.lessonname, teacher.fullname from student join balls on student.Studentid = balls.student\n" +
                "join lessons on balls.lesson = lessons.lessonId join teacher on teacher.teacherId = lessons.teacher";

        // select avg (balls.rating) as ratingAvg from balls where student = 1
        String getAvgBalls = "select avg (balls.rating) as ratingAvg from balls where student = 1";

        DataBase dataBase = new DataBase();
        dataBase.getStudentsInfo(getAllUsers);
        dataBase.getStudentsInfo(getUsersFromOneGroup);
        dataBase.getStudentsInfo(getUserOneDataYearOfReceipt);
        dataBase.getStudentsInfo(getLessonsWithBallsAndTeachers);
        dataBase.getStudentsInfo(getAvgBalls);
        

    }
}
