public class Main {
    public static void main(String[] args) {
        //Программа, которая находит среднее арифметическое значение двух чисел.
         System.out.println(Task1.getSumOfTwoNumbers(5,6));

        //Программа, которая находит среднее арифметическое значение
        // произвольного количества чисел.

       // System.out.println(Task2.getSum(args));

       /* Программу, которая предлагает пользователю ввести сумму денежного вклада в
        гривнах, процент годовых,которые выплачивает банк,
        длительность вклада (лет).Вывести накопленную сумму денег
        за каждый год и начисленные проценты.*/
       /* Task3 task3 = new Task3(100, 10, 2);
        task3.calculateSum();*/


    }

}

class Task1 {
    public static double getSumOfTwoNumbers(double a, double b) {
        return (a + b) / 2;
    }
}

class Task2 {
    public static double getSum(String[] args) {
        double sum = 0;
        for (int i = 0; i < args.length; i++) {
            sum += Integer.parseInt(args[i]);
        }
        return sum / args.length;
    }
}

class Task3 {
    private double sum;
    private double percentage;
    private int year;

    public Task3(double sum, double percentage, int year) {
        this.sum = sum;
        this.percentage = percentage;
        this.year = year;
    }

    private double getYearPercentage() {
        return this.sum * this.percentage / 100;
    }

    public void calculateSum() {
        for (int i = 1; i <= year; i++) {
            double percentage = getYearPercentage();
            System.out.println("Your percentage in " + i
                    + " year equals " + percentage);
            this.sum += percentage;
            System.out.println("Your  total sum in " + i + " year equals " + this.sum
                    + "\n");
        }
        System.out.println("Your whole sum equals " + this.sum);
    }
}

