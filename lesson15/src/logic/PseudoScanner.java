package logic;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PseudoScanner {
    Numbers num = new Numbers();
    Scanner scanner = new Scanner(System.in);
    private String addNumber;

    public PseudoScanner(){
        System.out.println("Введите число");
        addNumber = scanner.nextLine();
    }

    public void createArrayWithNumbers() {
        List<String[][]> list = new ArrayList<>();
        for (int i = 0; i < addNumber.length(); i++) {
            String number = String.valueOf(addNumber.charAt(i));
            if (!number.matches("\\b[0-9]+"))
                break;
            switch (number) {
                case "0":
                    list.add(num.getZero());
                    break;
                case "1":
                    list.add(num.getOne());
                    break;
                case "2":
                    list.add(num.getTwo());
                    break;
                case "3":
                    list.add(num.getThree());
                    break;
                case "4":
                    list.add(num.getFour());
                    break;
                case "5":
                    list.add(num.getFive());
                    break;
                case "6":
                    list.add(num.getSix());
                    break;
                case "7":
                    list.add(num.getSeven());
                    break;
                case "8":
                    list.add(num.getEight());
                    break;
                case "9":
                    list.add(num.getNine());
                    break;
            }
        }
        getFullArray(list);
    }

    private void getFullArray(List<String[][]> list){
        String[][] fullArray = new String[7][list.size() * 7];
        int destPos = 0;
        for (int i = 0; i < list.size(); i++) {
            String[][] temp = list.get(i);
            for (int j = 0; j < temp[i].length; j++) {
                System.arraycopy(temp[j], 0, fullArray[j], destPos, temp[j].length);
            }
            destPos+=7;
        }
        printArray(fullArray);

    }

    private void printArray(String[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j]);
            }
            System.out.println();
        }
    }

}
