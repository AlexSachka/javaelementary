package logic;

public class Numbers {
    private final String symbol = "*";
    private final String space = " ";
    //arrays 7/7
    public String[][] getZero() {
        return new String[][]{
                {symbol, symbol, symbol, symbol, symbol, symbol, space},
                {symbol, space, space, space, space, symbol, space},
                {symbol, space, space, space, space, symbol, space},
                {symbol, space, space, space, space, symbol, space},
                {symbol, space, space, space, space, symbol, space},
                {symbol, space, space, space, space, symbol, space},
                {symbol, symbol, symbol, symbol, symbol, symbol, space}
        };
    }

    public String[][] getOne() {
        return new String[][]{
                {space, space, space, symbol, space, space, space},
                {space, space, symbol, symbol, space, space, space},
                {space, symbol, space, symbol, space, space, space},
                {space, space, space, symbol, space, space, space},
                {space, space, space, symbol, space, space, space},
                {space, space, space, symbol, space, space, space},
                {space, symbol, symbol, symbol, symbol, symbol, space}
        };
    }

    public String[][] getTwo() {
        return new String[][]{
                {space, space, symbol, symbol, symbol, space, space},
                {space, symbol, space, space, space, symbol, space},
                {space, symbol, space, space, space, symbol, space},
                {space, space, space, space, symbol, space, space},
                {space, space, space, symbol, space, space, space},
                {space, space, symbol, space, space, space, space},
                {space, symbol, symbol, symbol, symbol, symbol, space}
        };
    }

    public String[][] getThree() {
        return new String[][]{
                {space, space, symbol, symbol, symbol, space, space},
                {space, symbol, space, space, space, symbol, space},
                {space, space, space, space, space, symbol, space},
                {space, space, space, symbol, symbol, space, space},
                {space, space, space, space, space, symbol, space},
                {space, symbol, space, space, space, symbol, space},
                {space, space, symbol, symbol, symbol, space, space}
        };
    }

    public String[][] getFour() {
        return new String[][]{
                {space, symbol, space, space, symbol, space, space},
                {space, symbol, space, space, symbol, space, space},
                {space, symbol, symbol, symbol, symbol, space, space},
                {space, space, space, space, symbol, space, space},
                {space, space, space, space, symbol, space, space},
                {space, space, space, space, symbol, space, space},
                {space, space, space, space, symbol, space, space}
        };
    }

    public String[][] getFive() {
        return new String[][]{
                {space, symbol, symbol, symbol, symbol, space, space},
                {space, symbol, space, space, space, space, space},
                {space, symbol, symbol, symbol, symbol, space, space},
                {space, space, space, space, space, symbol, space},
                {space, space, space, space, space, symbol, space},
                {space, symbol, space, space, space, symbol, space},
                {space, symbol, symbol, symbol, symbol, space, space}
        };
    }

    public String[][] getSix() {
        return new String[][]{
                {space, symbol, symbol, symbol, symbol, space, space},
                {space, symbol, space, space, symbol, space, space},
                {space, symbol, space, space, space, space, space},
                {space, symbol, symbol, symbol, symbol, space, space},
                {space, symbol, space, space, symbol, space, space},
                {space, symbol, space, space, symbol, space, space},
                {space, symbol, symbol, symbol, symbol, space, space}
        };
    }

    public String[][] getSeven() {
        return new String[][]{
                {symbol, symbol, symbol, symbol, symbol, symbol, space},
                {space, space, space, space, symbol, space, space},
                {space, space, space, symbol, space, space, space},
                {space, space, symbol, space, space, space, space},
                {space, symbol, space, space, space, space, space},
                {symbol, space, space, space, space, space, space},
                {space, space, space, space, space, space, space}
        };
    }

    public String[][] getEight() {
        return new String[][]{
                {space, symbol, symbol, symbol, symbol, symbol, space},
                {space, symbol, space, space, space, symbol, space},
                {space, symbol, space, space, space, symbol, space},
                {space, symbol, symbol, symbol, symbol, symbol, space},
                {space, symbol, space, space, space, symbol, space},
                {space, symbol, space, space, space, symbol, space},
                {space, symbol, symbol, symbol, symbol, symbol, space}
        };
    }

    public String[][] getNine() {
        return new String[][]{
                {space, symbol, symbol, symbol, symbol, symbol, space},
                {space, symbol, space, space, space, symbol, space},
                {space, symbol, space, space, space, symbol, space},
                {space, symbol, symbol, symbol, symbol, symbol, space},
                {space, space, space, space, space, symbol, space},
                {space, space, space, space, space, symbol, space},
                {space, symbol, symbol, symbol, symbol, symbol, space}
        };
    }

}
