import logic.Numbers;
import logic.PseudoScanner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        PseudoScanner ps = new PseudoScanner();
        ps.createArrayWithNumbers();
    }
}
