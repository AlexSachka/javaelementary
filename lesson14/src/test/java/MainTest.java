import org.junit.Assert;
import org.junit.Test;

public class MainTest {
    @Test
    public void testInsertSort() {
        int[] array = {3, 53, 12, 10};
        Algorithms.insertSort(array);
        Assert.assertArrayEquals("Array not sorted", new int[]{3, 10, 12, 53}, array);
        Assert.assertTrue("Array not sorted", Algorithms.insertSort(array));
    }

    @Test
    public void testInterpolationSearch() {
        int[] array = {1, 5, 8, 12, 15, 22};
        Assert.assertEquals("Element not fount in array",
                3, Algorithms.interpolationSearch(array, 12));
        Assert.assertEquals("Element not fount in array",
                -1, Algorithms.interpolationSearch(array, 33));
    }

    @Test
    public void testBubbleSort() {
        int[] array = {3, 53, 12, 10};
        Algorithms.bubbleSort(array);
        Assert.assertArrayEquals("Array isn't sorted", new int[]{3, 10, 12, 53}, array);
        Assert.assertTrue("Array isn't sorted", Algorithms.bubbleSort(array));
    }
}
