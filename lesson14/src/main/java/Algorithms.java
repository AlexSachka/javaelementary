public class Algorithms {

    public static boolean insertSort(int[] array) {
        int temp;
        int j;
        for (int i = 1; i < array.length; i++) {
            temp = array[i];
            for (j = i - 1; j >= 0 && array[j] > temp; j--) {
                array[j + 1] = array[j];
            }
            array[j + 1] = temp;
        }
        return true;
    }

    public static int interpolationSearch(int[] array, int element) {
        int startIndex = 0;
        int lastIndex = (array.length - 1);
        while ((startIndex <= lastIndex) && (element >= array[startIndex]) &&
                (element <= array[lastIndex])) {

            int index = startIndex + (((lastIndex - startIndex) /
                    (array[lastIndex] - array[startIndex])) *
                    (element - array[startIndex]));

            if (array[index] == element)
                return index;

            if (array[index] < element)
                startIndex = index + 1;

            else
                lastIndex = index - 1;
        }
        return -1;
    }

    public static boolean bubbleSort(int[] array) {
        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < array.length - 1; i++) {
                int temp = array[i + 1];
                if (array[i] > array[i + 1]) {
                    array[i + 1] = array[i];
                    array[i] = temp;
                    isSorted = false;
                }
            }
        }
        return true;
    }
}
