import implementation.XmlValidator;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import taskWithBrackets.Brackets;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;

public class Main {
    public static void main(String[] args) {
        File dir = new File("D:\\courseJavaElementary" +
                "\\lesson18\\src\\resources\\user.xml");
        XmlValidator validator = new XmlValidator();
        System.out.println(validator.xmlValidation(dir));

        //Task2
        String text = "{{}}}";
        Brackets brackets = new Brackets();
        System.out.println(brackets.isValid(text));
        System.out.println(brackets.isValid2(text));
    }
}
