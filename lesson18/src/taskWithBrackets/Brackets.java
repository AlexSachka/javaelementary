package taskWithBrackets;

import java.util.EmptyStackException;
import java.util.Stack;

public class Brackets {
    Stack<Character> stack;

    public boolean isValid(String text) {
        stack = new Stack<>();
        for (int i = 0; i < text.length(); i++) {
            char temp = text.charAt(i);
            if (temp == '{')
                stack.push(temp);
            else{
                try {
                    stack.pop();
                } catch (EmptyStackException e){
                    e.printStackTrace();
                }
            }

        }
        return stack.empty();
    }

    public boolean isValid2(String text){
        int count = 0;
        for (int i = 0; i < text.length() ; i++) {
            char temp = text.charAt(i);
            if (temp == '{')
                count++;
            else
                count--;
            if (count < 0)
                return false;
                break;
        }

        return count != 0 ? false : true;
    }

}
