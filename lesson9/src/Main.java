import linkedList.implementation.CustomLinkedList;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        CustomLinkedList list1 = new CustomLinkedList();
        list1.add("12");
        list1.add("13");
        list1.add("14");
        System.out.println(list1.size());
        System.out.println(list1.contains("12"));
        System.out.println(list1.contains2("12"));
        System.out.println(list1.get(1));
        list1.delete(1);
        System.out.println(list1.size());

    }
}
