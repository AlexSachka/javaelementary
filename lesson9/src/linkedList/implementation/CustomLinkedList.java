package linkedList.implementation;

import java.util.Iterator;


public class CustomLinkedList implements linkedList.interfaces.CustomCollection, Iterable {
    private int size = 0;
    private Node first;
    private Node last;

    @Override
    public boolean add(String str) {
        Node l = last;
        Node node = new Node(str, l, null);
        last = node;
        if (l == null) {
            first = node;
        } else {
            l.next = node;
        }
        size++;
        return true;
    }

    @Override
    public boolean delete(int index) {
        Node next = getNode(index).next;
        Node previous = getNode(index).previous;
        if (previous == null) {
            first = next;
        } else {
            previous.next = next;
            getNode(index).previous = null;
        }
        if (next == null) {
            last = previous;
        } else {
            next.previous = previous;
            getNode(index).next = null;
        }

        getNode(index).item = null;
        size--;
        return true;
    }
    private Node getNode(int index){
        Node node = first;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node;
    }

    @Override
    public String get(int index) {
        Node target = first;
        for (int i = 0; i < index; i++) {
            target = target.next;
        }
        return target.item;
    }

    @Override
    public boolean contains(String str) {
        Iterator<String> iterator = this.iterator();
        while (iterator.hasNext()) {
            if (str.equals(iterator.next()))
                return true;
        }
        return false;
    }

    // Второй способ контайнс
    public boolean contains2(String str) {
        Node target = first;
        for (int i = 0; i < size; i++) {
            if (target.item.equals(str)) {
                return true;
            }
            target = target.next;
        }
        return false;
    }

    @Override
    public int size() {
        return size;
    }

    // Поигрался с интерфейсом Iterable
    @Override
    public Iterator iterator() {
        return new Iterator() {
            int counter = 0;

            @Override
            public boolean hasNext() {
                return counter < size;
            }

            @Override
            public Object next() {
                return get(counter++);
            }
        };
    }

    private static class Node {
        String item;
        Node previous;
        Node next;

        public Node(String item, Node previous, Node next) {
            this.item = item;
            this.previous = previous;
            this.next = next;
        }
    }
}
