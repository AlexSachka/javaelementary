package linkedList.interfaces;

public interface CustomCollection {
    boolean add(String str);

    boolean delete (int index);


    String get(int index);

    boolean contains(String str);

    int size();

}
