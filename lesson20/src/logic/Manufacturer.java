package logic;

public class Manufacturer {
    DataBuffer buffer;

    public Manufacturer(DataBuffer buffer) {
        this.buffer = buffer;
    }

    public boolean addData(){
        for (int i = 0; i < buffer.array.length ; i++) {
            buffer.array[i] = i;
        }
        return true;
    }
}
