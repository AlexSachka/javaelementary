package logic;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Consumer {
    DataBuffer buffer;

    public Consumer(DataBuffer buffer) {
        this.buffer = buffer;
    }

    public void getData() {
        int[] array = new int[buffer.array.length];
        for (int i = 0; i < array.length; i++) {
            array[i] = buffer.array[i];
            System.out.println(array[i]);
        }
        buffer.array = new int[10];
    }
}
