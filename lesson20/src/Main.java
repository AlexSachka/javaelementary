import logic.Consumer;
import logic.DataBuffer;
import logic.Manufacturer;

public class Main {
    public static void main(String[] args) {
        DataBuffer buffer = new DataBuffer();
        Consumer consumer = new Consumer(buffer);
        Manufacturer manufacturer = new Manufacturer(buffer);

        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                synchronized (manufacturer){
                    manufacturer.addData();
                }
            }
        });
        Thread thread1 = new Thread(new Runnable(){
            @Override
            public void run() {
                synchronized (consumer){
                    consumer.getData();
                }

            }
        });
        thread.start();
        thread1.start();

    }
}
